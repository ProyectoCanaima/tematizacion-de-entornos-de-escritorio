Tematización de Entornos de Escritorios de Canaima GNU/Linux
=============================================================

El propósito de este espacio es presentar los archivos de tematización que pertenecen al **theme/Canaima** y **gnome-shell** ya que son los principales archivos resprensables de este proceso y la pieza clave de un buen Rebranding de sistema. Actualmente Canaima cuenta con dos (2) entornos que son: 

* Mate: Por ser un entorno de escriorio ligero para equipos de bajos recuersos con un diseño simple y tradicional. Siendo su principal atractivo el ser completamente Accesible para personas con discapacidad visual.
* GNOME: Característico por ser muy dinámico y distintos a todo el resto de entornos que existen en la actualidad e igualmente accesible. Un entorno dedicado a aquellas personas que les gusta la estáteca moderna reflejada en un espacio de estudio, trabajo o entretenimiento.

En la [wiki](https://gitlab.com/ProyectoCanaima/plymouth/-/wikis/home) de este proyecto, podrán encontrar documentación sobre como tematizar los entornos de escritorio antes mencionados, como colocarlos predeterminados y mucho más.
Antes de hacer uso de nuestros códigos, les sugerimos que lean el archivo [LICENSE.md](https://gitlab.com/ProyectoCanaima/tematizacion-de-entornos-de-escritorio/-/blob/master/LICENSE) que contiene todas los términos  y condiciones de la Licencia Pública Genereal de GNU.
